# README #

### What is this repository for? ###

This project is an implementation of a PRNG using the sponge construction.

The permutation function used is keccak block permutation
http://en.wikipedia.org/wiki/SHA-3#The_block_permutation



* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

Ce projet est réalisé par Manuel Soulatre, Karim Chahal, Corentin Soriot et Tahar Bennacef
pour toute question contactez nous:
Manuel Soulatre : msoulatre[at]gmail[dot]com
Corentin Soriot : corentin[at]soriot[dot]fr 
Tahar Bennacef  : bennacef.tahar[at]gmail[dot]com
Karim Chahal    : --

* Other community or team contact
