CC=gcc
CFLAGS=-W -Wall $(OPTLEVEL)
BINFOLDER=./bin
SRCFOLDER=./src
OBJFOLDER=./obj
OPTLEVEL=-O3

all: $(BINFOLDER)/sprnge

$(BINFOLDER)/sprnge: $(OBJFOLDER)/sprnge.o $(OBJFOLDER)/keccak-f.o
	$(CC) -o $(BINFOLDER)/sprnge $(OBJFOLDER)/sprgne.o $(OBJFOLDER)/keccak-f.o $(CFLAGS)

$(OBJFOLDER)/sprnge.o: $(SRCFOLDER)/sprnge.c $(SRCFOLDER)/keccak-f.h
	$(CC) -o $(OBJFOLDER)/sprgne.o -c $(SRCFOLDER)/sprnge.c $(CFLAGS)

$(OBJFOLDER)/keccak-f.o: $(SRCFOLDER)/keccak-f.c $(SRCFOLDER)/keccak-f.h
	$(CC) -o $(OBJFOLDER)/keccak-f.o -c $(SRCFOLDER)/keccak-f.c $(CFLAGS)
	
clean:
	rm -rf $(OBJFOLDER)/*

cleanall: clean
	rm -rf $(BINFOLDER)/*
