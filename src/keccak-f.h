

#define W 100
#define matrix(m,i,j,k) m[((i * 5 + j) * W + k)]
#define CAPACITY 64             // en octet garantit la sécurité de notre PRNG en 2^(c*8/2).  L'augmenter augmente la sécurité mais reduit les perf
#define RATE  (200-CAPACITY)
#define L 6                     // log2( |mot| ) ici log2(64)
#define NBROUNDS (12 + 2*L)

#define ROTL64(x, y) (((x) << (y)) ^ ((x) >> (64 - (y))))
//#define SSE

void keccakf(uint64_t state[5][5]);

extern void* s_memset(void* buffer, unsigned char c, int bytes_size);
