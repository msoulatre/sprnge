/****************
 * Sprnge Project
 *
 * Authors :
 * Manuel Soulatre (msoulatre@gmail.com)
 * Tahar Bennacef (bennacef.tahar@gmail.com)
 * Chahal Karim (chahal.karim1@gmail.com)
 * Corentin Soriot (corentin.soriot@etudiant.univ-rennes1.fr)
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include "keccak-f.h"

/* xorer: xor one buffer data1 with another data2 on n bytes */
void xorer(void* data1, const void* data2, int bytes_size) {
  int i;
  for(i = 0 ; i < bytes_size ; i++) {
    *(((char*) data1) + i) = *(((char*) data1) + i) ^ *(((char*) data2) + i);
  }
}

void display_buffer(const void* data, int bytes_size) {
  int i;
  for(i = 0 ; i < bytes_size ; i++) {
    printf("%02x", *((unsigned char*)data + i));
  }
  puts("");
}

void* s_memset(void* buffer, unsigned char c, int bytes_size) {
  volatile unsigned char *work_buffer = buffer;
  while(bytes_size--) {
    *(work_buffer++) = c;
  }
  return buffer;
}

// absorbtion : out == NULL
// squeeze    :  in == NULL
void duplex(const void* in, int in_size, void* out, uint64_t state[5][5]){
  if(out != NULL) {
    keccakf(state);
    memcpy(out, state, RATE);
  }
  if(in != NULL) {
    xorer(state, in, in_size);
    keccakf(state);
  }
}

int main() {
  uint64_t state[5][5];
  uint8_t input_buffer[RATE];
  uint8_t output_buffer[RATE];
  char user_input;
  FILE* input_file;
  
  input_file = fopen("/dev/urandom", "r");
  
  if(input_file == NULL) {
    perror("i/o error");
    return -1;
  }
  
  puts("Manual mod (r: refresh, f: fetch, x: exit)");
  
  s_memset(state, 0x0, sizeof(state));
  
  while((user_input = getchar()) != 'x') {
    if(user_input == 'r') {
      puts("> Refresh");
      fread(input_buffer, 1, RATE, input_file);
      duplex(input_buffer, RATE, NULL, state);
    }
    else if(user_input == 'f') {
      puts("> Fetch");
      duplex(NULL, 0, output_buffer, state);
      display_buffer(output_buffer, RATE);
    }
  }
  
  puts("Program ended");
  s_memset(state, 0x0, sizeof(state));
  fclose(input_file);
  
  return 0;
}
