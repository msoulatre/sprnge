/**********
 * Sprnge Project
 *
 * Authors :
 * Manuel Soulatre (msoulatre@gmail.com)
 * Tahar Bennacef (bennacef.tahar@gmail.com)
 * Chahal Karim (chahal.karim1@gmail.com)
 * Corentin Soriot (corentin@soriot.fr)
 *
 * Permutation de keccak
 * 
 */

#include <stdint.h>
#include "keccak-f.h"

#ifdef SSE
#include <xmmintrin.h>
#endif

#define R00 0
#define R10 36
#define R20 3
#define R30 41
#define R40 18

#define R01 1
#define R11 44
#define R21 10
#define R31 45
#define R41 2

#define R02 62
#define R12 6
#define R22 43
#define R32 15
#define R42 61

#define R03 28
#define R13 55
#define R23 25
#define R33 21
#define R43 56

#define R04 27
#define R14 20
#define R24 39
#define R34 8
#define R44 14

static const uint64_t RC[24] =
{
  0x0000000000000001, 0x0000000000008082, 0x800000000000808a,
  0x8000000080008000, 0x000000000000808b, 0x0000000080000001,
  0x8000000080008081, 0x8000000000008009, 0x000000000000008a,
  0x0000000000000088, 0x0000000080008009, 0x000000008000000a,
  0x000000008000808b, 0x800000000000008b, 0x8000000000008089,
  0x8000000000008003, 0x8000000000008002, 0x8000000000000080,
  0x000000000000800a, 0x800000008000000a, 0x8000000080008081,
  0x8000000000008080, 0x0000000080000001, 0x8000000080008008
};

void keccakf(uint64_t state[5][5]) {
  uint64_t temp_state[5];
  uint64_t temp;
  uint8_t round;
  
  for(round=0; round < NBROUNDS; round++) {
    // theta (parity mix)
    temp_state[0] = state[0][0] ^ state[1][0] ^ state[2][0] ^ state[3][0] ^ state[4][0];
    temp_state[1] = state[0][1] ^ state[1][1] ^ state[2][1] ^ state[3][1] ^ state[4][1];
    temp_state[2] = state[0][2] ^ state[1][2] ^ state[2][2] ^ state[3][2] ^ state[4][2];
    temp_state[3] = state[0][3] ^ state[1][3] ^ state[2][3] ^ state[3][3] ^ state[4][3];
    temp_state[4] = state[0][4] ^ state[1][4] ^ state[2][4] ^ state[3][4] ^ state[4][4];

    temp = temp_state[4] ^ ROTL64( temp_state[1], 1) ;
    state[0][0] ^= temp;
    state[1][0] ^= temp;
    state[2][0] ^= temp;
    state[3][0] ^= temp;
    state[4][0] ^= temp;

    temp = temp_state[0] ^ ROTL64( temp_state[2], 1) ;
    state[0][1] ^= temp;
    state[1][1] ^= temp;
    state[2][1] ^= temp;
    state[3][1] ^= temp;
    state[4][1] ^= temp;

    temp = temp_state[1] ^ ROTL64( temp_state[3], 1) ;
    state[0][2] ^= temp;
    state[1][2] ^= temp;
    state[2][2] ^= temp;
    state[3][2] ^= temp;
    state[4][2] ^= temp;

    temp = temp_state[2] ^ ROTL64( temp_state[4], 1) ;
    state[0][3] ^= temp;
    state[1][3] ^= temp;
    state[2][3] ^= temp;
    state[3][3] ^= temp;
    state[4][3] ^= temp;

    temp = temp_state[3] ^ ROTL64( temp_state[0], 1) ;
    state[0][4] ^= temp;
    state[1][4] ^= temp;
    state[2][4] ^= temp;
    state[3][4] ^= temp;
    state[4][4] ^= temp;

    // rho (rotation) & pi (mix lane)
    temp = state[0][1];
    state[0][1] = ROTL64(state[1][1], R11);  
    state[1][1] = ROTL64(state[1][4], R14);
    state[1][4] = ROTL64(state[4][2], R42);
    state[4][2] = ROTL64(state[2][4], R24);
    state[2][4] = ROTL64(state[4][0], R40);
    state[4][0] = ROTL64(state[0][2], R02);
    state[0][2] = ROTL64(state[2][2], R22);
    state[2][2] = ROTL64(state[2][3], R23);
    state[2][3] = ROTL64(state[3][4], R34);
    state[3][4] = ROTL64(state[4][3], R43);
    state[4][3] = ROTL64(state[3][0], R30);
    state[3][0] = ROTL64(state[0][4], R04);
    state[0][4] = ROTL64(state[4][4], R44);
    state[4][4] = ROTL64(state[4][1], R41);
    state[4][1] = ROTL64(state[1][3], R13);
    state[1][3] = ROTL64(state[3][1], R31);
    state[3][1] = ROTL64(state[1][0], R10);
    state[1][0] = ROTL64(state[0][3], R03);
    state[0][3] = ROTL64(state[3][3], R33);
    state[3][3] = ROTL64(state[3][2], R32);
    state[3][2] = ROTL64(state[2][1], R21);
    state[2][1] = ROTL64(state[1][2], R12);
    state[1][2] = ROTL64(state[2][0], R20);
    state[2][0] = ROTL64(temp,        R01);

    // chi
    temp_state[0] = state[0][0];
    temp_state[1] = state[0][1];
    state[0][0] ^= ((~state[0][1])   & state[0][2]);
    state[0][1] ^= ((~state[0][2])   & state[0][3]);
    state[0][2] ^= ((~state[0][3])   & state[0][4]);
    state[0][3] ^= ((~state[0][4])   & temp_state[0]);
    state[0][4] ^= ((~temp_state[0]) & temp_state[1]);

    temp_state[0] = state[1][0];
    temp_state[1] = state[1][1];
    state[1][0] ^= ((~state[1][1])   & state[1][2]);
    state[1][1] ^= ((~state[1][2])   & state[1][3]);
    state[1][2] ^= ((~state[1][3])   & state[1][4]);
    state[1][3] ^= ((~state[1][4])   & temp_state[0]);
    state[1][4] ^= ((~temp_state[0]) & temp_state[1]);

    temp_state[0] = state[2][0];
    temp_state[1] = state[2][1];
    state[2][0] ^= ((~state[2][1])   & state[2][2]);
    state[2][1] ^= ((~state[2][2])   & state[2][3]);
    state[2][2] ^= ((~state[2][3])   & state[2][4]);
    state[2][3] ^= ((~state[2][4])   & temp_state[0]);
    state[2][4] ^= ((~temp_state[0]) & temp_state[1]);

    temp_state[0] = state[3][0];
    temp_state[1] = state[3][1];
    state[3][0] ^= ((~state[3][1])   & state[3][2]);
    state[3][1] ^= ((~state[3][2])   & state[3][3]);
    state[3][2] ^= ((~state[3][3])   & state[3][4]);
    state[3][3] ^= ((~state[3][4])   & temp_state[0]);
    state[3][4] ^= ((~temp_state[0]) & temp_state[1]);

    temp_state[0] = state[4][0];
    temp_state[1] = state[4][1];
    state[4][0] ^= ((~state[4][1])   & state[4][2]);
    state[4][1] ^= ((~state[4][2])   & state[4][3]);
    state[4][2] ^= ((~state[4][3])   & state[4][4]);
    state[4][3] ^= ((~state[4][4])   & temp_state[0]);
    state[4][4] ^= ((~temp_state[0]) & temp_state[1]);

    // iota
    state[0][0] ^= RC[round];
  }
  s_memset(&temp, 0, sizeof(temp));
  s_memset(temp_state, 0, sizeof(temp_state));
}
